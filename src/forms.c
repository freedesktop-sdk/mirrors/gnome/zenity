/* vim: colorcolumn=80 ts=4 sw=4
 */
/*
 * forms.c
 *
 * Copyright © 2010 Arx Cruz
 * Copyright © 2021-2023 Logan Rathbone
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 121 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 * Original Author: Arx Cruz <arxcruz@gnome.org>
 */

#include "util.h"
#include "zenity.h"

#include <string.h>

#include <config.h>

static ZenityData *zen_data;
static GSList *selected;
static void zenity_forms_dialog_response (GtkWidget *widget, char *rstr, gpointer data);

static void
zenity_forms_dialog_get_selected (GtkTreeModel *model, GtkTreePath *path_buf,
	GtkTreeIter *iter, gpointer data)
{
	int n_columns = 0;
	GValue value = G_VALUE_INIT;
	GtkTreeView *tree_view = GTK_TREE_VIEW(data);

	g_return_if_fail (GTK_IS_TREE_VIEW (tree_view));

	n_columns = gtk_tree_model_get_n_columns (model);

	for (int i = 0; i < n_columns; ++i)
	{
		gtk_tree_model_get_value (model, iter, i, &value);
		selected = g_slist_append (selected, g_value_dup_string (&value));
		g_value_unset (&value);
	}
}

static GtkWidget *
zenity_forms_create_and_fill_combo (ZenityFormsData *forms_data,
		int combo_number)
{
	g_autoptr(GtkListStore) list_store = NULL;
	GtkWidget *combo_box;
	GtkCellRenderer *renderer;

	list_store = gtk_list_store_new (1, G_TYPE_STRING);

	if (forms_data->combo_values)
	{
		g_autofree char *combo_values =
			g_slist_nth_data (forms_data->combo_values, combo_number);

		if (combo_values)
		{
			char **row_values = g_strsplit_set (combo_values, "|", -1);

			if (row_values)
			{
				int i = 0;
				GtkTreeIter iter;
				char *row = row_values[i];

				while (row != NULL)
				{
					gtk_list_store_append (list_store, &iter);
					gtk_list_store_set (list_store, &iter, 0, row, -1);
					row = row_values[++i];
				}
				g_strfreev (row_values);
			}
		}
	}

	combo_box = gtk_combo_box_new_with_model (GTK_TREE_MODEL(list_store));

	renderer = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT(combo_box),
			renderer,
			TRUE);	/* gboolean expand */
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT(combo_box), renderer,
			"text", 0,
			NULL);

	return combo_box;
}

static GtkWidget *
zenity_forms_create_and_fill_list (ZenityFormsData *forms_data,
		int list_number, char *header)
{
	g_autoptr(GtkListStore) list_store = NULL;
	GtkWidget *tree_view;
	GtkWidget *scrolled_window;
	GType *column_types = NULL;

	int i = 0;
	/* If no column names available, default is one */
	int n_columns = 1;
	int column_index = 0;

	tree_view = gtk_tree_view_new ();

	if (forms_data->column_values)
	{
		char *column_values;
		int columns_values_count = g_slist_length (forms_data->column_values);
		int column_number = 0;

		if (list_number < columns_values_count) {
			column_number = list_number;
		}

		column_values = g_slist_nth_data (forms_data->column_values,
				column_number);

		if (column_values)
		{
			char **values = g_strsplit_set (column_values, "|", -1);

			if (values)
			{
				n_columns = g_strv_length (values);
				column_types = g_new (GType, n_columns);

				for (i = 0; i < n_columns; i++)
					column_types[i] = G_TYPE_STRING;

				for (i = 0; i < n_columns; i++)
				{
					GtkCellRenderer *renderer;
					GtkTreeViewColumn *column;
					char *column_name = values[i];

					renderer = gtk_cell_renderer_text_new ();
					column =
						gtk_tree_view_column_new_with_attributes (column_name,
								renderer,
								"text", column_index,
								NULL);
					gtk_tree_view_append_column (GTK_TREE_VIEW (tree_view),
							column);
					column_index++;
				}
			}
		}
		else
		{
			/* If no values available, add one with string type*/
			column_types = g_new (GType, n_columns);
			column_types[0] = G_TYPE_STRING;
		}
	}

	list_store = g_object_new (GTK_TYPE_LIST_STORE, NULL);

	gtk_list_store_set_column_types (list_store, n_columns, column_types);

	if (forms_data->list_values)
	{
		g_autofree char *list_values =
			g_slist_nth_data (forms_data->list_values, list_number);

		if (list_values)
		{
			char **row_values = g_strsplit_set (list_values, "|", -1);

			if (row_values)
			{
				GtkTreeIter iter;
				char *row = row_values[0];
				int position = -1;
				i = 0;

				while (row != NULL)
				{
					if (position >= n_columns || position == -1)
					{
						position = 0;
						gtk_list_store_append (list_store, &iter);
					}
					gtk_list_store_set (list_store, &iter, position, row, -1);
					position++;
					row = row_values[++i];
				}
				g_strfreev (row_values);
			}
		}
	}

	gtk_tree_view_set_model (GTK_TREE_VIEW (tree_view),
			GTK_TREE_MODEL (list_store));
	gtk_tree_view_set_headers_visible (
		GTK_TREE_VIEW (tree_view), forms_data->show_header);

	scrolled_window = gtk_scrolled_window_new ();
	gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW(scrolled_window),
			tree_view);
	gtk_widget_set_size_request (scrolled_window, -1, 100);

	return scrolled_window;
}

void
zenity_forms_dialog (ZenityData *data, ZenityFormsData *forms_data)
{
	g_autoptr(GtkBuilder) builder = NULL;
	GtkWidget *dialog;
	GtkWidget *grid;
	GtkWidget *text;

	int list_count = 0;
	int combo_count = 0;
	int i = 0;

	zen_data = data;

	builder = zenity_util_load_ui_file ("zenity_forms_dialog", "zenity_forms_box", NULL);

	if (builder == NULL) {
		data->exit_code = zenity_util_return_exit_code (ZENITY_ERROR);
		return;
	}

	dialog = GTK_WIDGET(gtk_builder_get_object (builder,
				"zenity_forms_dialog"));

	g_signal_connect (dialog, "response",
		G_CALLBACK (zenity_forms_dialog_response), forms_data);

	if (data->dialog_title)
		adw_message_dialog_set_heading (ADW_MESSAGE_DIALOG(dialog), data->dialog_title);;

	if (data->width > -1 || data->height > -1) {
		gtk_window_set_default_size (GTK_WINDOW(dialog),
				data->width, data->height);
	}

	if (data->extra_label)
	{
		ZENITY_UTIL_ADD_EXTRA_LABELS (dialog)
	}

	if (data->ok_label)
	{
		ZENITY_UTIL_SETUP_OK_BUTTON_LABEL (dialog);
	}

	if (data->cancel_label)
	{
		ZENITY_UTIL_SETUP_CANCEL_BUTTON_LABEL (dialog);
	}

	text = GTK_WIDGET (gtk_builder_get_object (builder, "zenity_forms_text"));

	if (forms_data->dialog_text)
		gtk_label_set_markup (
			GTK_LABEL (text), g_strcompress (forms_data->dialog_text));

	grid = GTK_WIDGET (gtk_builder_get_object (builder, "zenity_forms_grid"));

	i = 0;
	for (GSList *tmp = forms_data->list;
			tmp != NULL;
			tmp = tmp->next)
	{
		ZenityFormsValue *zenity_value = tmp->data;
		GtkWidget *label;

		label = gtk_label_new (zenity_value->option_value);
		gtk_widget_set_halign (label, GTK_ALIGN_START);
		gtk_grid_attach (GTK_GRID (grid), label, 0, i, 1, 1);

		switch (zenity_value->type)
		{
			case ZENITY_FORMS_ENTRY:
				zenity_value->forms_widget = gtk_entry_new ();
				break;

			case ZENITY_FORMS_PASSWORD:
				zenity_value->forms_widget = gtk_entry_new ();
				gtk_entry_set_visibility (GTK_ENTRY(zenity_value->forms_widget),
						FALSE);
				break;

			case ZENITY_FORMS_CALENDAR:
				zenity_value->forms_widget = gtk_calendar_new ();
				break;

			case ZENITY_FORMS_LIST:
				zenity_value->forms_widget =
					zenity_forms_create_and_fill_list (forms_data,
							list_count, zenity_value->option_value);
				list_count++;
				break;

			case ZENITY_FORMS_COMBO:
				zenity_value->forms_widget =
					zenity_forms_create_and_fill_combo (forms_data,
							combo_count);
				combo_count++;
				break;

			default:
				zenity_value->forms_widget = gtk_entry_new ();
				break;
		}

		gtk_grid_attach_next_to (GTK_GRID (grid),
			GTK_WIDGET (zenity_value->forms_widget),
			label,
			GTK_POS_RIGHT,
			1,
			1);

		gtk_accessible_update_relation (GTK_ACCESSIBLE (zenity_value->forms_widget),
			GTK_ACCESSIBLE_RELATION_LABELLED_BY,
			label,
			NULL,
			-1);

		++i;
	}

	zenity_util_show_dialog (dialog);

	if (data->timeout_delay > 0)
	{
		g_timeout_add_seconds (data->timeout_delay,
			(GSourceFunc) zenity_util_timeout_handle,
			dialog);
	}
	zenity_util_gapp_main (GTK_WINDOW(dialog));
}

static void
zenity_forms_dialog_output (ZenityFormsData *forms_data)
{
	GSList *tmp, *tmp2;
	guint day, year, month;
	g_autoptr(GDate) date = NULL;
	char time_string[128];
	g_autofree char *combo_value = NULL;
	GtkTreeSelection *selection;
	g_autoptr(GtkListStore) list_store = NULL;
	GtkTreeIter iter;

	for (tmp = forms_data->list; tmp; tmp = tmp->next)
	{
		ZenityFormsValue *zenity_value = tmp->data;

		switch (zenity_value->type)
		{
			case ZENITY_FORMS_PASSWORD:
			case ZENITY_FORMS_ENTRY:
				g_print ("%s",
						gtk_entry_buffer_get_text (gtk_entry_get_buffer
							(GTK_ENTRY(zenity_value->forms_widget))));
				break;

			case ZENITY_FORMS_LIST:
				selection =
					gtk_tree_view_get_selection
						(GTK_TREE_VIEW(gtk_scrolled_window_get_child
						   (GTK_SCROLLED_WINDOW(zenity_value->forms_widget))));

				gtk_tree_selection_selected_foreach (selection,
					zenity_forms_dialog_get_selected,
					GTK_TREE_VIEW(gtk_scrolled_window_get_child
						(GTK_SCROLLED_WINDOW(zenity_value->forms_widget))));

				for (tmp2 = selected; tmp2; tmp2 = tmp2->next)
				{
					if (tmp->next != NULL) {
						g_print ("%s,", (char *)tmp2->data);
					} else
						g_print ("%s", (char *)tmp2->data);
				}

				g_slist_free_full (g_steal_pointer (&selected), g_free);
				break;

			case ZENITY_FORMS_CALENDAR:
				g_object_get (zenity_value->forms_widget,
						"day", &day,
						"month", &month,
						"year", &year,
						NULL);
				date = g_date_new_dmy (day, month + 1, year);
				g_date_strftime (time_string, sizeof time_string, forms_data->date_format, date);
				g_print ("%s", time_string);
				break;

			case ZENITY_FORMS_COMBO:
				if (gtk_combo_box_get_active_iter
						(GTK_COMBO_BOX(zenity_value->forms_widget), &iter))
				{
					list_store =
						GTK_LIST_STORE(gtk_combo_box_get_model
								(GTK_COMBO_BOX(zenity_value->forms_widget)));

					gtk_tree_model_get (GTK_TREE_MODEL(list_store),
						&iter,
						0,
						&combo_value,
						-1);

					g_print ("%s", combo_value);
				}
				else
					g_print (" ");
				break;

		}
		if (tmp->next != NULL)
			g_print ("%s", forms_data->separator);
	}
	g_print ("\n");
}

static void
zenity_forms_dialog_response (GtkWidget *widget, char *rstr, gpointer data)
{
	ZenityFormsData *forms_data = data;
	ZenityExitCode response = zenity_util_parse_dialog_response (rstr);

	switch (response)
	{
		case ZENITY_OK:
			zenity_forms_dialog_output (forms_data);
			zenity_util_exit_code_with_data (ZENITY_OK, zen_data);
			break;

		case ZENITY_CANCEL:
			zen_data->exit_code = zenity_util_return_exit_code (ZENITY_CANCEL);
			break;

		case ZENITY_TIMEOUT:
			zenity_forms_dialog_output (forms_data);
			zen_data->exit_code = zenity_util_return_exit_code (ZENITY_TIMEOUT);
			break;

		case ZENITY_ESC:
			zen_data->exit_code = zenity_util_return_exit_code (ZENITY_ESC);
			break;

		default:
			if (zen_data->extra_label &&
				response < (int)g_strv_length (zen_data->extra_label))
				printf ("%s\n", zen_data->extra_label[response]);
			zen_data->exit_code = zenity_util_return_exit_code (ZENITY_ESC);
			break;
	}
	zenity_util_gapp_quit (GTK_WINDOW(widget), zen_data);
}
